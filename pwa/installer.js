(function() {
	if (!navigator.serviceWorker.controller) {
		navigator.serviceWorker.register('pwabuilder-sw.js', { scope: './' });
	}
	
	let pwaInstall;
	const installButton = document.getElementById('install');
	
	window.addEventListener('beforeinstallprompt', (e) => {
		e.preventDefault();
		pwaInstall = e;
		installButton.hidden = false;
	});
	
	installButton.addEventListener('click', (e) => {
		installButton.hidden = true;
		pwaInstall.prompt();
	});
})();