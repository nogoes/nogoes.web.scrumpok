new Vue({
	el: '#v',
	data: {
		views: [
			{ id: 'list', label: 'List', isActive: true },
			{ id: 'text', label: 'Text', isActive: false },
			{ id: 'number', label: 'Number', isActive: false },
			{ id: 'slide', label: 'Slider', isActive: false },
			{ id: 'steps', label: 'Steps', isActive: false }
		],
		cards: [
			0,
			1,
			2,
			3,
			5,
			8,
			13,
			20,
			40,
			100
		],
		card: 0,
		fullscreen: false
	},
	computed: {
		activeView() {
			return this.views.find(x => x.isActive);
		},
		cardIndex() {
			return this.cards.findIndex(x => x === +this.card);
		}
	},
	methods: {
		toggleView(view) {
			const current = this.activeView;
			current.isActive = false;

			view.isActive = true;
			this.$nextTick(() => {
				this.focusInput(view);
			});
		},
		focusInput(view) {
			const ref = this.$refs[view.id];
			ref && ref.focus();
		},
		decrement() {
			this.card = this.cards[this.cardIndex - 1];
		},
		increment() {
			this.card = this.cards[this.cardIndex + 1];
		},
		correctRange(e) {
			const v = e.target.value;

			if (this.cards.includes(v)) {
				this.card = v;
				return;
			}

			this.card = this.cards.reduce((acc, x) => {
				const pad = Math.abs(x - v);
				return acc.pad < pad ? acc : {
					pad: pad,
					value: x
				};
			}, { value: 0 }).value;
		}
	},
	mounted() {
		this.card = this.cards[~~(this.cards.length * Math.random())];
	}
});